module.exports = {
  verified: [
    {
      name: "Plugin 1",
      author: "github-username",
      description: "Some Description to be published.",
      installation: "https://url.to/some/INSTALL.md",
      repository: "https://github.com/some-public-repository1",
      keywords: ["word1", "word2", "word3"]
    },
    {
      name: "Plugin 2",
      author: "github-username",
      description: "Some Description to be published.",
      installation: "https://url.to/some/INSTALL.md",
      repository: "https://github.com/some-public-repository2",
      keywords: ["word1", "word2", "word3"]
    },
  ],
  submitted: [
    {
      name: "Plugin 3",
      author: "github-username",
      description: "Some Description to be published.",
      installation: "https://url.to/some/INSTALL.md",
      repository: "https://github.com/some-public-repository3",
      keywords: ["word1", "word2", "word3"]
    },
    {
      name: "Plugin 4",
      author: "github-username",
      description: "Some Description to be published.",
      installation: "https://url.to/some/INSTALL.md",
      repository: "https://github.com/some-public-repository4",
      keywords: ["word1", "word2", "word3"]
    },
  ]
};
